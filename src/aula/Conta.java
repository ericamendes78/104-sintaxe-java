package aula;

public class Conta {
	
	private int agencia;
	private int numero;
	private int digitoVerificador;
	private double saldo;
	private String tipo;
	
	public Conta(int agencia, int numero, int digitoVerificador, String tipo) {
		this.agencia = agencia;
		this.numero = numero;
		this.digitoVerificador = digitoVerificador;
		this.tipo = tipo;
		saldo = 0;
	}
	
	public boolean sacar(double valor) {
		if(saldo >= valor) {
			saldo -= valor;
			return true;
		}
		
		return false;
	}
	
	public void depositar(double valor) {
		saldo += valor;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public String toString() {
		return agencia + " | " + numero + "-" + digitoVerificador; 
	}
}
