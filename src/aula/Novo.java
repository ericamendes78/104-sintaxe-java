package aula;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Novo {
	public static void main(String[] args) {
		//Exemplo de uso de uma classe
		Conta contaDoSilvioSantos = new Conta(4422, 12345, 1, "Private");
		
		contaDoSilvioSantos.depositar(10000.0);
		
		System.out.println(contaDoSilvioSantos);
		System.out.println(contaDoSilvioSantos.getSaldo());
		
		//Exemplo de Set
		HashSet<String> pokemons = new HashSet<>();
		
		pokemons.add("Pikachu de Fogo");
		pokemons.add("Pikachu de Água");
		pokemons.add("Pikachu de Planta");
		pokemons.add("Pikachu de Planta");
		
		System.out.println(pokemons.contains("Pikachu de Planta"));
		System.out.println(pokemons);
		
		//Exemplo de Lista
		ArrayList<String> linguagensQueEuManjo = new ArrayList<>();
		linguagensQueEuManjo.add("C");
		linguagensQueEuManjo.add("Cobol");
		linguagensQueEuManjo.add("Osasquês");
		linguagensQueEuManjo.add("Assembler");
		linguagensQueEuManjo.add("Fortran");
		linguagensQueEuManjo.add("Arnold C");
		linguagensQueEuManjo.remove(2);
		
		System.out.println(linguagensQueEuManjo.size());
		
		for(String linguagem: linguagensQueEuManjo) {
			System.out.println(linguagem);
		}
	
		//Exemplo de Map
		HashMap<String, String> pessoa = new HashMap<>();
		
		pessoa.put("nome", "Renan");
		pessoa.put("profissao", "Professor");
		
		System.out.println(pessoa.containsKey("signo"));//retorna false
		
		System.out.println(pessoa.get("nome"));
		
		//Exemplo de Lambda
		linguagensQueEuManjo.forEach(linguagem -> {
			System.out.println(linguagem);
		});
		
	}
}
